variable "access_key" {
  type = string
  sensitive = false
}

variable "secret_key" {
  type = string
  sensitive = true
}

provider "aws" {
  region = "us-east-1"
  access_key = var.access_key
  secret_key = var.secret_key
}

resource "aws_key_pair" "my_key_pair" {
    key_name = "my_key"
    public_key = file("/home/pheng/.ssh/id_rsa.pub")
}

resource "aws_security_group" "security" {
  name = "fproj-ssh"
  description = "allow ssh traffic"

  # ssh ingress
  ingress {
    cidr_blocks = [
      "0.0.0.0/0"   # open to all IPs
    ]
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "fproj" {
  ami           = "ami-04b70fa74e45c3917"
  instance_type = "t2.medium"            # t3 for 2cores (k8)
  count         = 1
  vpc_security_group_ids = [aws_security_group.security.id]
  associate_public_ip_address = true
  key_name      = aws_key_pair.my_key_pair.key_name
  tags = {
    Name = "Devops final-${count.index}"
  }
}
