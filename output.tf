output "aws_instances" {

  value = [for instance in aws_instance.fproj : instance.public_ip]

}