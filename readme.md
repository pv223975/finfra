# readme.md

# import TF variables (export TF_VAR_access_key, TF_VAR_secret_key, TF_VAR_public_key):
source .vars

# run Terraform  -  Note outputIP
terraform plan
terraform apply

# test ec2 connectivity:
ssh ubuntu@[outputIP]

# test ansible connectivity using private key:
ansible all -i hosts.yml -m ping

# ansible playbook to install minikube/kubectl on hosts:
ansible-playbook k8-playbook.yml -i hosts.yml
